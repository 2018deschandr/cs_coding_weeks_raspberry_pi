# Fonctionnalité 1 : Charger et afficher une image avec OpenCV

L'objectif de cette première partie est de prendre en main la bibliothèque [OpenCV](https://opencv.org/) qui est la bibliothèque de référence en vision par ordinateur et qui est [open-source](https://sourceforge.net/projects/opencvlibrary/).
Nativement écrite en C, puis C++, elle a des interfaces en plusieurs languages dont python. Une documentation relativement complète d'OpenCV python est disponible [ici](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html).


## Etape 1 : Installer OpenCV

Pour installer OpenCV, le plus simple est soit d'utiliser PyCharm Edu pour ajouter la bibliothèque OpenCV à votre projet soit d'executer la commande ci-dessous sur un terminal :

` pip install opencv `

Sous réserve que l'utilitaire `pip` soit installé sur votre ordinateur.

Pour tester la bonne installation d'OpenCV, ouvrer une console python (vous pouvez le faire avec PyCharmEdu) et tapez les commandes ci-dessous:

```
import cv2
print(cv2.__version__)
```

Si tout se passe bien, c'est que vous avez bien installé OpenCV. Si non, alors, essayez de trouver ce qui ne vas pas. Un guide complet autour de l'installation d'OpenCV est disponible [ici](https://www.pyimagesearch.com/opencv-tutorials-resources-guides/).


## Etape 2 : le Hello World de OpenCV

Dans cette étape nous allons utiliser OpenCV pour lire et pour afficher une image.
 
 + Ajouter un module `utils_cv` à votre projet.
 + Créer un répertoire `Data` à la racine de votre projet et ajouter y l'[image](./Images/tetris_blocks.png) ci-dessous.

 ![Tetris](./Images/tetris_blocks.png)

Il s'agit maintenant d'écrire le code permettant de charger et d'afficher cette image. On définira pour cela la fonction `load_and_display_image(filenale)` dans le module `utils_cv`.

Pour vous aider, vous pouvez regarder [cette partie](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html#display-image) du tutorial d'OpenCV.


Vous avez maintenant terminé la fonctionnalité 1.


## A propos de la gestion des versions

<span style='color:blue'> Pour toute la suite du projet, il vous est demandé de :</span> 

+ <span style='color:blue'>Faire un commit dès que la réalisation d'une fonctionnalité ou d'une sous-fonctionnalité est finie.</span> 
+ <span style='color:blue'>Tagger à la fin de chaque journée votre dernier commit </span> 
+ <span style='color:blue'>Pousser (Push) votre code vers votre dépôt distant sur GitLab.</span> 
+ <span style='color:blue'>Faire un test de couverture de code à la fin de chaque journée et de pousser le bilan obtenu vers votre dépôt distant sur GitLab.</span>



Après avoir mis à jour votre dépôt git local et distant,vous pouvez maintenant passer à la [**Fonctionnalité 2** : Effectuer un traitement sur une image et afficher le résultat du traitement.](./Facerecognition_S1_traitement.md)









