# Prise en main de votre Raspberry


Pour ce projet, avant même de commencer les développements, il faut démarrer avec votre nano-ordinateur.


Votre première journée va donc être consacrée à cela :

 - Déballer et mettre en marche votre Raspberry Pi
 	-	[Premiers pas](https://www.raspberrypi-france.fr/premiere-utilisation-raspberry-pi/)
 	- [Premiers pas](https://alain-michel.canoprof.fr/eleve/tutoriels/raspberry/premiers-pas-raspberrypi/index.xhtml)
 	- [Guide débutant](https://the-raspberry.com/guide-debutant)
 	- ...
 
 
 - L'utiliser de manière correcte
	 - 	[Diffuser l'écran raspberry sur un pc portable](https://forums.framboise314.fr/viewtopic.php?t=3384)
 	- [Diffuser l'écran raspberry sur un pc portable](http://mycoolpizza.blogspot.com/2013/07/raspberry-pi-connecter-directement-sur.html)
 
 - Utiliser la caméra
	 - [Premiers pas avec la caméra ](https://projects.raspberrypi.org/en/projects/getting-started-with-picamera).
	 - [OpenCV](https://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/)

	 
Vous pouvez maintenant commencez votre projet par  [**Fonctionnalité 1** : Charger et afficher une image.](./Facerecognition_S1_displayimage.md)	 